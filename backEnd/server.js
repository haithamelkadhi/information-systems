const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');

const app = express();

// Middleware
app.use(bodyParser.json());
app.use(cors());

// Configuration de l'URI de la base de données
const dblog = "mongodb://127.0.0.1:27017/crmProject"; 

// Fonction de connexion à la base de données
async function connectDB(mongoDB) {
  try {
    await mongoose.connect(mongoDB, {
      
    });
    console.log("L'api s'est connectée à MongoDB.");
  } catch (err) {
    console.error("L'api n'a pas réussi à se connecter à MongoDB : ", err);
    process.exit(1);
  }
}

// Connexion à MongoDB
connectDB(dblog);

// Import des routes
const dataController = require('./controllers/data');

// Utilisation des routes
app.use('/api/data', dataController);

// Démarrage du serveur
const PORT = process.env.PORT || 5000;
app.listen(PORT, () => {
  console.log(`Le serveur fonctionne sur le port ${PORT}`);
});
