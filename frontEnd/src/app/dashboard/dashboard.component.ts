import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data/data.service';
import { Candidate } from '../models/Candidate';
import { env } from 'src/environments/environment';



@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  data: any;

  constructor(private http: HttpClient, private dataService: DataService) { }
  filterAgent = [
    { label: 'All Agents', value: null },
    ...env.agentDropdown
  ]
  filterSource = [
    { label: 'All Source', value: null },
    ...env.sourceDropdown
  ]
  filterGender = [
    { label: 'All Gender', value: null },
    ...env.genderDropdown
  ]
  filterCountry = [
    { label: 'All Country', value: null },
    ...env.countryDropdown
  ]
  candidates: Candidate[] = []

  ngOnInit(): void {
    this.dataService.getData().subscribe(
      (res) => {
        console.log('Response Back End')
        console.log(res)
        this.candidates = res
      },
      (error) => {
        console.error('Error fetching Data:', error);
      }
    );
  }

}