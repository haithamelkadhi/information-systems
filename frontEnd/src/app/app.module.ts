import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HttpClientModule } from '@angular/common/http';
import { MainmenuComponent } from './mainmenu/mainmenu.component';
import { WholeAppComponent } from './whole-app/whole-app.component';
import { TableModule } from 'primeng/table';
import { TagModule } from 'primeng/tag';
import { InputTextModule } from 'primeng/inputtext';
import { MultiSelectModule } from 'primeng/multiselect';
import { DropdownModule } from 'primeng/dropdown';
import { FormComponent } from './form/form.component';
import { ReportingComponent } from './reporting/reporting.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CalendarModule } from 'primeng/calendar';
import { ToastModule } from 'primeng/toast';
import { MessageService } from 'primeng/api';
import { DatePipe } from '@angular/common';
import { ButtonModule } from 'primeng/button';
@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    MainmenuComponent,
    WholeAppComponent,
    FormComponent,
    ReportingComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    TableModule,
    TagModule,
    MultiSelectModule,
    InputTextModule,
    DropdownModule,
    FormsModule,
    ReactiveFormsModule,
    ButtonModule,
    CalendarModule,
    ToastModule
  ],
  providers: [
    MessageService,
    DatePipe,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
