export class Candidate {
    public constructor(
        public _id?: string,
        public gender?: string,
        public name?: string,
        public family_name?: string,
        public birthdate?: Date,
        public country?: string,
        public agent?: string,
        public source?: string,
        public campus?: string,
        public field?: string,
        public candidacy_evaluation?: string,
        public language?: string,
        public decision?: string,
        public date_creation?: Date
    ) { };
}