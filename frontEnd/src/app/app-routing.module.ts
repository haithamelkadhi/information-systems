import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { FormComponent } from './form/form.component';
import { ReportingComponent } from './reporting/reporting.component';

const routes: Routes = [
  { path: '', component: DashboardComponent},
  { path: 'add', component: FormComponent},
  { path: 'reporting', component: ReportingComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
