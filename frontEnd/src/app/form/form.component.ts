import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { DataService } from '../services/data/data.service';
import { Candidate } from '../models/Candidate';
import { MessageService } from 'primeng/api';
import { env } from 'src/environments/environment';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent {
  formAdd: FormGroup = new FormGroup({
    gender: new FormControl(),
    name: new FormControl(),
    family_name: new FormControl(),
    birthdate: new FormControl(),
    country: new FormControl(),
    agent: new FormControl(),
    source: new FormControl(),
    campus: new FormControl(),
    field: new FormControl(),
    candidacy_evaluation: new FormControl(),
    language: new FormControl(),
    decision: new FormControl(),
    candidacy_date: new FormControl()
  })

  genderDropdown = env.genderDropdown;
  agentDropdown = env.agentDropdown
  campusDropdown = env.campusDropdown
  countryDropdown = env.countryDropdown
  sourceDropdown = env.sourceDropdown
  fieldDropdown = env.fieldDropdown
  candidacyDropdown = env.candidacyDropdown
  languageDropdown = env.langugageDropdown
  decisionDropdown = env.decisionDropdown

  constructor(private ApiService: DataService, private ToastService: MessageService) { }

  onSubmit() {
    this.ApiService.create({ ...this.formAdd.value }).subscribe(r => {
      this.ToastService.add({ severity: 'success', summary: 'Ajout avec succès' })
      this.formAdd.reset()
    }, error => {
      console.error(error)
      this.ToastService.add({ severity: 'error', summary: 'Erreur lors de l\'ajout', detail: error.error.toString() })
    })
  }
}
