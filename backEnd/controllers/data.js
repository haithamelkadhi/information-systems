const express = require('express');
const app = express();
const { Candidate } = require('../models/candidate')

// Test route
app.get('/getData', (req, res) => {
  Candidate.find().then(candidates => {
    res.send(candidates)
  })
});

app.post('/create', (req, res) => {
  let candidate = new Candidate({ ...req.body, date_creation: new Date() })
  candidate.save()
    .then((response) => { res.status(201).send(response); })
})


module.exports = app;
