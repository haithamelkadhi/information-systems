const mongoose = require("mongoose");
const candidate_schema = new mongoose.Schema({
    gender: { type: String },
    name: { type: String },
    family_name: { type: String },
    birthdate: { type: Date },
    country: { type: String },
    agent: { type: String },
    source: { type: String },
    campus: { type: String },
    field: { type: String },
    candidacy_evaluation: { type: String },
    language: { type: String },
    decision: { type: String },
    date_creation: { type: Date },
    candidacy_date: { type: Date },
});

const Candidate = mongoose.model("candidate", candidate_schema);
module.exports = { Candidate };