import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { env } from 'src/environments/environment';
import { Candidate } from 'src/app/models/Candidate';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private apiUrl = env.origin + 'data';

  constructor(private http: HttpClient) { }

  getData(): Observable<Candidate[]> {
    return this.http.get<Candidate[]>(`${this.apiUrl}/getData`);
  }

  create(data: Candidate): Observable<Candidate> {
    return this.http.post<Candidate>(`${this.apiUrl}/create`, data);
  }
}