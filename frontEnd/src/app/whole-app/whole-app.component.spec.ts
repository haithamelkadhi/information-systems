import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WholeAppComponent } from './whole-app.component';

describe('WholeAppComponent', () => {
  let component: WholeAppComponent;
  let fixture: ComponentFixture<WholeAppComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [WholeAppComponent]
    });
    fixture = TestBed.createComponent(WholeAppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
