import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-mainmenu',
  templateUrl: './mainmenu.component.html',
  styleUrls: ['./mainmenu.component.scss']
})
export class MainmenuComponent {

  constructor(private route: Router) {}
  navigateTo(url:string) {
    this.route.navigateByUrl(url)
  }
}
